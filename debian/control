Source: ruby-httparty
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Cédric Boutillier <boutil@debian.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb (>= 1),
               rake,
               ruby,
               help2man,
               ruby-mini-mime (>= 1.0.0),
               ruby-multi-xml (>= 0.5.2),
               ruby-rspec,
               ruby-webmock,
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-httparty.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-httparty
Homepage: https://github.com/jnunemaker/httparty
Testsuite: autopkgtest-pkg-ruby
Rules-Requires-Root: no

Package: ruby-httparty
Architecture: all
Depends: ${misc:Depends},
         ${ruby:Depends},
         ${shlibs:Depends},
Breaks: ruby-gitlab (<< 4.16.1~)
Description: quick web service consumption from any Ruby class
 HTTParty is a Ruby library to build easily classes that can use
 Web-based APIs and related services. At its simplest, the
 HTTParty module is included within a class, which gives the class a
 "get" method that can retrieve data over HTTP. Further directives,
 however, instruct HTTParty to parse results (XML, JSON, and so on),
 define base URIs for the requests, and define HTTP authentication
 information.
